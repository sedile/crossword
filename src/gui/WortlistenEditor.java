package gui;

import java.awt.BorderLayout;
import java.awt.event.ActionListener;
import java.awt.event.FocusListener;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionListener;

import steuerung.Tupel;

/**
 * Der <code>WortlistenEditor</code> stellt die graphische Benutzeroberflaeche 
 * fuer den Dialog zum Editieren von Wortlisten dar. Hier hat man die Auswahl
 * zunaechst eine Wortliste zu laden oder zu editieren und man kann ein Raetsel
 * generieren lassen. Es ist das Startfenster dieser Anwendung.
 */
public class WortlistenEditor extends JFrame {
	
	/* Swing Komponenten */
	private JMenuItem[] menufeld = new JMenuItem[4];
    private JButton[] buttonfeld = new JButton[6];
    private JTextField wortsuchetextfeld;
    private JLabel wortsuche,generatorlabel;
    
    /** zur Anzeige und Auswahl der einzelnen Woertern */
    private JList<String> wortliste;
    
    /** verwaltet die Eintraege einer JList */
    private DefaultListModel<String> wortlistenmodell;
    
    /**
     * Konstruktor fuer den WortlistenEditor. Hier wird das Fenster
     * aufgebaut.
     */
    public WortlistenEditor() {
        setTitle("Wortliste");
        komponentenInit();
        erzeugeMenubar();
        erzeugeFenster();
        pack();
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLocation(getAufloesung().getLinks(),getAufloesung().getRechts());
        setVisible(true);
    }

    /**
     * Fuegt allen Swing-Komponenten einen Listenerobjekt hinzu
     * @param action	Ein Actionlistener-Objekt
     * @param llistener	Ein ListSelectionListener-Objekt
     * @param focus		Ein Focuslistener-Objekt
     */
    private void listenerInit(ActionListener action, ListSelectionListener llistener, FocusListener focus){
        wortsuchetextfeld.addActionListener(action);
    	for(byte i = 0; i < buttonfeld.length; i++){
    		buttonfeld[i].addActionListener(action);
    	}
    	for(byte j = 0; j < menufeld.length; j++){
    		menufeld[j].addActionListener(action);
    	}
    	
        wortliste.addListSelectionListener(llistener);
        
        wortsuchetextfeld.addFocusListener(focus);
    }
    
    /**
     * hier werden alle Komponenten initialisiert
     */
    private void komponentenInit(){
        menufeld[0] = new JMenuItem("Neue Liste");
        menufeld[1] = new JMenuItem("Liste laden");
        menufeld[2] = new JMenuItem("Liste speichern");
        menufeld[3] = new JMenuItem("SchlieÃen");
        wortsuche = new JLabel("Wortsuche");
        wortsuchetextfeld = new JTextField(25);
        generatorlabel = new JLabel("Raetsel generieren per : ");
        buttonfeld[0] = new JButton("Hinzufuegen");
        buttonfeld[1] = new JButton("Bearbeiten");
        buttonfeld[2] = new JButton("Loeschen");
        buttonfeld[3] = new JButton("loesche Liste");
        buttonfeld[4] = new JButton("interaktiv");
        buttonfeld[5] = new JButton("zufall");
    }
    
    /**
     * erzeuge die Menueleiste samt aller Eintraege der JMenuItems
     */
    private void erzeugeMenubar() {
        JMenuBar menubar = new JMenuBar();
        JMenu listenmenu = new JMenu("Wortlisten");
        menubar.add(listenmenu);
        listenmenu.add(menufeld[0]);
        listenmenu.add(menufeld[1]);
        listenmenu.add(menufeld[2]);
        listenmenu.add(menufeld[3]);       
        setJMenuBar(menubar);
    }

    /**
     * hier wird das Aussehen des Fensters erstellt.
     */
    private void erzeugeFenster() {
        setLayout(new BorderLayout());
        wortlistenmodell = new DefaultListModel<String>();
        wortliste = new JList<String>(wortlistenmodell);
        
        /* Jedes Wort soll nur einzelnt auswaehlbar sein */
        wortliste.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        wortliste.setSelectedIndex(0);
        wortliste.setVisibleRowCount(20);
        
        /* Panel, um die Liste und Wortfilter hinzuzufuegen */
        JPanel listenPanel = new JPanel();
        listenPanel.setLayout(new BoxLayout(listenPanel, BoxLayout.Y_AXIS));
        JPanel filterPanel = new JPanel();
        filterPanel.add(wortsuche);
        filterPanel.add(wortsuchetextfeld);
        listenPanel.add(filterPanel);
        listenPanel.add(new JScrollPane(wortliste));
        add(listenPanel, BorderLayout.WEST);
        
        /* Alle Buttons die mit der Wortliste im zusammenhang stehen */
        JPanel buttonPanel = new JPanel(new BorderLayout());
        JPanel listenbuttons = new JPanel();
        listenbuttons.add(buttonfeld[0]);
        listenbuttons.add(buttonfeld[1]);
        listenbuttons.add(buttonfeld[2]);
        listenbuttons.add(buttonfeld[3]);
        buttonPanel.add(listenbuttons);
        add(listenbuttons, BorderLayout.NORTH);
        JPanel raetselbuttons = new JPanel();
        raetselbuttons.add(generatorlabel);
        raetselbuttons.add(buttonfeld[4]);
        raetselbuttons.add(buttonfeld[5]);
        buttonPanel.add(raetselbuttons);
        add(buttonPanel, BorderLayout.SOUTH);

        /* Erst muss eine Liste geladen werden */
        buttonfeld[1].setEnabled(false);
        buttonfeld[2].setEnabled(false);
        buttonfeld[3].setEnabled(false);
        buttonfeld[4].setEnabled(false);
        buttonfeld[5].setEnabled(false);
    }
    
    /**
     * hier wird eine neue Wortliste erzeugt und die
     * Woerter werden der neu erzeugten Liste hinzugefuegt
     * 
     * @param woerter eine ArrayList mit den Loesungswoertern
     */
    public final void neuesListenmodell(ArrayList<String> woerter) {
        wortlistenmodell.clear();
        
        /* Fuege alle Woerter in das Listenmodell ein */
        for(int i = 0; i < woerter.size(); i++) {
            wortlistenmodell.addElement(woerter.get(i));
        }
    }
    
    /**
     * uebergibt von der Steuerklasse die Listener, um die Swing-Komponenten des
     * Wortlisteneditors bedienen zu koennen
     * @param action	ActionListener
     * @param llistener	ListSelectionListener
     * @param focus		FocusListener
     */
    public void setListener(ActionListener action, ListSelectionListener llistener, FocusListener focus){
    	listenerInit(action,llistener,focus);
    }
    
    /**
     * gibt die JList zurueck
     * @return wortliste als JList
     */
    public JList<String> getWortliste(){
    	return wortliste;
    }
    
    /**
     * gibt das Listenmodell zur JList zurueck
     * @return listenmodell
     */
    public DefaultListModel<String> getWortlistenmodell(){
    	return wortlistenmodell;
    }
    
    /** Gibt die Kompontene des Wortsuchetextfeldes zurueck
     * 
     * @return wortsuchetextfeld
     */
    public JTextField getWortsucheTextfeld(){
    	return wortsuchetextfeld;
    }
    
    /** Das Fenster oeffnet sich in der Mitte des Bildschirms */
    private Tupel<Integer> getAufloesung(){
    	return new Tupel<Integer>(400,300);
    }
    
    /**
     * gibt das Feld der JButtons zurueck um auf die
     * einzelnen Buttons zuzugreifen
     * @return Feld, wobei 0 = neu, 1 = edit, 2 = wort loeschen, 3 = liste loeschen
     * 4 = generator mit auswahl, 5 = generator mit zufall
     */
    public JButton[] getButtons(){
    	return buttonfeld;
    }
    
    /**
     * gibt das Feld der JMenuItems zurueck
     * @return Feld, wobei 0 = neue Liste, 1 = liste laden, 2 = liste speichern, 3 = beenden
     */
    public JMenuItem[] getMenueintraege(){
    	return menufeld;
    }
}
