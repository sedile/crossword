package gui;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.WindowConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import steuerung.Tupel;
import ausnahmen.BeschreibungException;
import ausnahmen.KeinWortException;
import ausnahmen.SyntaxException;

/**
 * Hier werden verschiedene Moeglichkeiten angeboten Loesungswoerter
 * und/oder Beschreibungen zu editieren. Diese veraendert dementsprechend
 * den Inhalt der Wortliste.
 */
public class WortbeschreibungEditor extends JDialog {
	
	/** aktuelles Loesungswprt */
    private String loesungswort = "";
    
    /** Loesungswortbeschreibungen */
    private HashSet<String> beschreibungenSet = new HashSet<String>();
    private boolean speichern = false;
	
    /** Feld enthaelt JButtons, wobei 0 = hinzufuegen, 1 = aendern, 2 = loeschen, 3 = bestaetigen, 4 = beenden */
    private JButton[] buttonfeld = new JButton[5];
    
    /** zur Anzeige und Auswahl der einzelnen Woertern */
    private JList<String> beschreibungsliste;
    
    /** verwaltet die Eintraege einer JList */
    private DefaultListModel<String> beschreibungsListenmodell;
    
    /** Um Loesungswort und Beschreibung einzugeben */
    private JTextField wortTextfeld, wortbeschreibungsfeld;
      
    /**
     * erzeugt ein Fenster ohne Listeninhalte zum Anlegen eines neuen Loesungswortes
     * 
     * @param frame Instanz von der Klasse <code>WortlistenEditor</code> aus
     * <code>Steuerklasse</code>, damit das Startfenster nicht weiter bedient werden kann.
     */
    public WortbeschreibungEditor(JFrame frame) {
        super(frame, "Wortbeschreibungs-Editor", true);
        initKomponenten();
        erzeugeFenster();
        pack();
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setLocation(getAufloesung().getLinks(),getAufloesung().getRechts());
        
        /* innere Klasse stellt den Controller dieser Klasse dar */
        BeschreibungListener listener = new BeschreibungListener();
        beschreibungsliste.setFocusable(false);
        listenerInit(listener, listener, listener);
    }
    
    /**
     * erzeugt ein Fenster mit den uebergebenen Werten zum Editieren eines bestehenden
     * Loesungswortes oder zum editieren einer seiner Beschreibungen
     * 
     * @param lWort Das Loesungswort was evtl. veraendert wird
     * @param beschreibung Die Beschreibungen des Loesungsowrtes als HashSet was evtl. veraendert wird
     * @param jf analog wie beim anderen Konstruktor
     */
    public WortbeschreibungEditor(JFrame jf, String lWort, HashSet<String> beschreibung) {
        this(jf);
        loesungswort = lWort;
        beschreibungenSet = beschreibung;
        
        /* ausgewaehltes Loesungswort, was angezeigt wird */
        wortTextfeld.setText(loesungswort);
        wortTextfeld.setEditable(false);
        
        /* Suche zu diesem Loesungswort seine Beschreibung(en) und fuege Sie den
         * Wortbeschreibungslistenmodell zu */
        for (Iterator<String> it = beschreibungenSet.iterator(); it.hasNext();) {
            beschreibungsListenmodell.addElement(it.next());
        }
        
        /* freigegeben fuer das veraendern */
        beschreibungsliste.setFocusable(true);
    }
    
    /**
     * initialisiert die JButtons
     */
    private void initKomponenten(){
        buttonfeld[0] = new JButton("Hinzufuegen");
        buttonfeld[1] = new JButton("aendern");
        buttonfeld[2] = new JButton("Loeschen");
        buttonfeld[3] = new JButton("Bestaetigen");
        buttonfeld[4] = new JButton("Abbrechen");
    }
    
    /**
     * Fuegt allen Swing-Komponenten einen Listenerobjekt hinzu
     * @param action	Ein Actionlistener-Objekt
     * @param llistener	Ein ListSelectionListener-Objekt
     * @param focus		Ein Focuslistener-Objekt
     */
    private void listenerInit(ActionListener action, ListSelectionListener llistener, FocusListener focus){
    	for(byte i = 0; i < buttonfeld.length; i++){
    		buttonfeld[i].addActionListener(action);
    	}
        wortTextfeld.addActionListener(action);
        wortbeschreibungsfeld.addActionListener(action);
        
        beschreibungsliste.addListSelectionListener(llistener);
        
        wortTextfeld.addFocusListener(focus);
        wortbeschreibungsfeld.addFocusListener(focus);      
    }
    
    /**
     * hier wird das Aussehen des Fensters erstellt
     */
    private void erzeugeFenster() {
        setLayout(new BorderLayout());
        wortTextfeld = new JTextField(25);
       
        JPanel wortPanel = new JPanel();
        wortPanel.setLayout(new BoxLayout(wortPanel, BoxLayout.Y_AXIS));
        wortPanel.add(new JLabel("Loesungswort :"));
        wortPanel.add(wortTextfeld);
        add(wortPanel, BorderLayout.NORTH);
        
        beschreibungsListenmodell = new DefaultListModel<String>();
        beschreibungsliste = new JList<String>(beschreibungsListenmodell);
        beschreibungsliste.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        beschreibungsliste.setSelectedIndex(0);
        beschreibungsliste.setVisibleRowCount(5);
        JPanel beschreibungPanel = new JPanel();
        beschreibungPanel.setLayout(new BorderLayout());
        beschreibungPanel.add(new JScrollPane(beschreibungsliste), BorderLayout.CENTER);
        beschreibungPanel.add(new JLabel("Wortbeschreibungen zum Loesungswort: "), BorderLayout.NORTH);
        wortbeschreibungsfeld = new JTextField(30);
        beschreibungPanel.add(wortbeschreibungsfeld, BorderLayout.SOUTH);
        add(beschreibungPanel, BorderLayout.CENTER);
        
        JPanel beschreibungButtonPanel = new JPanel();
        beschreibungButtonPanel.setLayout(new BoxLayout(beschreibungButtonPanel, BoxLayout.Y_AXIS));
        beschreibungButtonPanel.add(buttonfeld[0]);
        beschreibungButtonPanel.add(buttonfeld[1]);
        beschreibungButtonPanel.add(buttonfeld[2]);
        beschreibungButtonPanel.add(buttonfeld[3]);
        beschreibungButtonPanel.add(buttonfeld[4]);
        add(beschreibungButtonPanel, BorderLayout.EAST);   
        
        buttonfeld[1].setEnabled(false);
        buttonfeld[2].setEnabled(false);
        buttonfeld[3].setEnabled(false);            
    }
    
    /**
     * hier wird ein neues DefaultListModel erstellt und mit den uebergebenen Woertern
     * aufgefuellt. Das Listenmodell enthaelt die Beschreibungen zu einem Loesungswort
     * 
     * @param woerter ArrayList mit den ganzen Wortbeschreibungen zu den aktuellen Loesungswort
     */
    private void neuesBeschreibungslistenmodell(ArrayList<String> woerter) {
        beschreibungsListenmodell.clear();
        
        /* Fuege die Beschreibungen die zu einem Loesungswort gehoeren
         * dem Wortbeschreibungslistenmodell hinzu */
        for(int i = 0; i < woerter.size(); i++) {
            beschreibungsListenmodell.addElement(woerter.get(i));
        }
    }
    
    /**
     * Prueft ob die Eingegebenen Wortbeschreibungn ok sind
     * 
     * @param beschreibungen Beschreibungslaenge > 0 und <= 60 Zeichen
     * @throws BeschreibungException wenn eine Beschreibung leer oder laenger als 60 Zeichen ist
     */
    private void pruefeWortBeschreibungen(Set<String> beschreibungen) throws BeschreibungException {
        if(beschreibungen.isEmpty() == true) {
            throw new BeschreibungException(beschreibungen.size());
        } else {
        	for (Iterator<String> it = beschreibungen.iterator(); it.hasNext();) {
        	
        		/* Prueft die gefundenen Beschreibungen */
        		pruefeWortBeschreibung(it.next());
        	}
        }
    }
    
    /**
     * Prueft ob die neue Wortbeschreibung die eingegeben wurde in Ordnung ist
     * 
     * @param beschreibung Laenge > 0 und <= 60 Zeichen
     * @throws BeschreibungException wenn eine Beschreibung leer oder laenger als 60 Zeichen ist
     */
    private void pruefeWortBeschreibung(String beschreibung) throws BeschreibungException {
        int laenge = beschreibung.length();

        if (laenge == 0 || laenge > 60) {
            throw new BeschreibungException(laenge);
        }
    }
    
    /**
     * Das uebergebene Loesungswort wird ueberprueft, ob es Zeichen enthaelt und notfalls
     * werden die einzelnen Zeichen standardisiert. Alle kleinbuchstaben werden zu
     * Grossbuchstaben und Umlaute werden umgewandelt. Sonderzeichen gelten als
     * Verstoss und werden nicht akzeptiert.
     * 
     * @param loesungswort Das Loesungswort was ueberprueft wird.
     * @return Das standardisiertes Loesungswort, falls moeglich
     * @throws KeinWortException keine Zeichen enthalten
     * @throws SyntaxException  unzulaessige Zeichen
     */
    private String pruefeLoesungswort(String loesungswort) throws KeinWortException, SyntaxException {
        if(loesungswort.length() == 0) {
            throw new KeinWortException("Kein gueltiges Loesungswort");
        }
        loesungswort = loesungswort.toUpperCase();
        loesungswort = loesungswort.replace("Ã", "AE");
        loesungswort = loesungswort.replace("Ö", "OE");
        loesungswort = loesungswort.replace("Ü", "UE");
        loesungswort = loesungswort.replace("ss", "SS");
        loesungswort = loesungswort.replace("0", "O");
        for (int i = 0;i<loesungswort.length();i++) {
            char buchstabe = loesungswort.charAt(i);
            int zeichen = buchstabe;
            
            /* Nur Grossbuchstaben "zwischen" A-Z sind zulaessig
             * sonst fliegt eine SyntaxException */
            if (zeichen < 65 | zeichen > 90) {
                throw new SyntaxException(buchstabe);
            }
        }
        return loesungswort;
    }
    
    /**
     * pruefe, ob die Eingabe des Loesungswortes gueltig ist
     * 
     * @param textfeld Das Loesungswort
     */
    private void pruefeEingegebenesWort(JTextField textfeld) {
        String wort = textfeld.getText();
        try {
            wort = pruefeLoesungswort(wort);
            
            /* Falls Wort ungueltig ist, ersetzte das Wort durch
             * das gueltige Loesungswort was geaendert wurde */
            textfeld.setText(wort);
            loesungswort = wort;
        }
        catch (KeinWortException e1) {
        	JOptionPane.showMessageDialog(null, "Kein Wort eingegeben: " + e1.getMessage(), "Loesungswort", JOptionPane.ERROR_MESSAGE);
        }
        catch (SyntaxException e2) {
            JOptionPane.showMessageDialog(null, "Wort enthaelt unzulaessiges Zeichen: " + e2.getMessage(), "Loesungswort", JOptionPane.ERROR_MESSAGE);
        }

    }
    
    /**
     * prï¿½fe, ob die Eingabe der Wortbeschreibung gueltig ist
     * 
     * @param textfeld Loesungswortbeschreibung
     */
    private void pruefeBeschreibung(JTextField textfeld) {
        String beschreibung = textfeld.getText();
        
        /* Es muss eine Beschreibung existieren, sonst ungueltig */
        if (beschreibung != "") {
            try {
                pruefeWortBeschreibung(beschreibung);
            }
            catch (BeschreibungException e1) {
                JOptionPane.showMessageDialog(null, String.format("Beschreibung enthaelt %s Zeichen, maximal 60 zulaessig", e1.getMessage()), "Beschreibung", JOptionPane.ERROR_MESSAGE);
            }
        }

    }
    
    /**
     * hier wird eine neue Wortbeschreibung zu den aktuellen
     * Loesungswort hinzugefuegt
     */
    private void neueWortbeschreibung() {
        String beschreibung = wortbeschreibungsfeld.getText();
        if (beschreibung.length() == 0) {
            JOptionPane.showMessageDialog(null, "Beschreibung ist leer", "Beschreibung", JOptionPane.ERROR_MESSAGE);                           
            return;
        }
        try {
        	
        	/* Pruefe Beschreibung */
            pruefeWortBeschreibung(beschreibung);
            
            /* Fuege die gueltige Beschreibung dem Set zu */
            beschreibungenSet.add(beschreibung);
            
            ArrayList<String> beschreibungliste = new ArrayList<String>();
            beschreibungliste.addAll(beschreibungenSet);
            neuesBeschreibungslistenmodell(beschreibungliste);
            beschreibungsliste.setSelectedValue(beschreibung, true);
            
            /* Beschreibung koennte veraendert werden */
            buttonfeld[1].setEnabled(true);
            if (beschreibungenSet.size() > 1) {
            	buttonfeld[2].setEnabled(true);
            }
            buttonfeld[3].setEnabled(true);
            beschreibungsliste.setFocusable(true);
        }
        catch (BeschreibungException e1) {
            JOptionPane.showMessageDialog(null, String.format("Beschreibung enthaelt %s Zeichen, maximal 60 zulaessig", e1.getMessage()), "Beschreibung", JOptionPane.ERROR_MESSAGE);            
        }
    }
    
    /**
     * veraendert die Beschreibung die ausgewaehlt wurde. Somit wird
     * eine bestehende Beschreibung ersetzt
     */
    private void editBeschreibung() {
    	String neueBeschreibung = wortbeschreibungsfeld.getText();
    	String alteBeschreibung = beschreibungsliste.getSelectedValue();
        if (neueBeschreibung != alteBeschreibung) {
        	
        	/* entferne zuerst die ausgewaehlte Wortbeschreibung und fuege die
        	 * neue Wortbeschreibung hinzu */
            beschreibungenSet.remove(alteBeschreibung);
            beschreibungenSet.add(neueBeschreibung);
            int listenindex = beschreibungsliste.getSelectedIndex();
            
            /* entferne die Wortbeschreibung aus den Beschreibungslistenmodell und fuege die
             * aktuelle Wortbeschreibung hinzu */
            beschreibungsListenmodell.remove(listenindex);
            beschreibungsListenmodell.add(listenindex, neueBeschreibung);
            beschreibungsliste.setSelectedIndex(listenindex);
            buttonfeld[3].setEnabled(true);
        }
    }
    
    /**
     * hier wird die ausgewaehlte Wortbeschreibung aus den Listenmodell und
     * aus den HashSet geloescht
     */
    private void loescheBeschreibung() {
    	String alteBeschreibung = beschreibungsliste.getSelectedValue();
    	int index = beschreibungsliste.getSelectedIndex();
    	/* loesche die nicht mehr gewollte Wortbeschreibung aus dem Set und
    	 * aus der Wortbeschreibungslistenmodell */
        beschreibungenSet.remove(alteBeschreibung);
        beschreibungsListenmodell.remove(index);
        buttonfeld[3].setEnabled(true);
    }

    /**
     * pruefe, ob die Eingabe korrekt war und bestaetige die Eingabe
     */
    private void bestaetigen() {
        try {
        	
        	/* wenn Loesungswort und Beschreibung in Ordnung sind */
            pruefeLoesungswort(loesungswort);
            pruefeWortBeschreibungen(beschreibungenSet);
            speichern = true;
            setVisible(false);               
        }
        catch (KeinWortException e1) {
            JOptionPane.showMessageDialog(null, "Bitte ein Loesungswort eingeben.", "Loesungswort", JOptionPane.ERROR_MESSAGE);
        }
        catch (SyntaxException e2) {
            JOptionPane.showMessageDialog(null, "Wort enthaelt unzulaessiges Zeichen: " + e2.getMessage(), "Loesungswort", JOptionPane.ERROR_MESSAGE);
        }
        catch (BeschreibungException e3) {
            JOptionPane.showMessageDialog(null, "Beschreibung ist leer oder enthaelt mehr als 60 Zeichen.", "Beschreibung", JOptionPane.ERROR_MESSAGE);            
        }
    }

    /**
     * breche den Dialog ab und schliesse das aktuelle Fenster
     */
    private void abbrechen() {
        speichern = false;
        setVisible(false);
        dispose();
    } 
    
    /**
     * gibt zurueck, ob die Eingaben akzeptiert werden
     * 
     * @return true, alles ok, sonst false
     */
    public boolean getErgebnis() {
        return speichern;
    }
    
    /**
     * hier wird das Loesungswort zurueckgegeben
     * 
     * @return loesungswort Das Loesungswort
     */
    public String getLoesungswort() {
        return loesungswort;
    }
    
    /**
     * ermittelt die Beschreibungen zu einem Loesungswort und gibt 
     * sie als HashSet zurueck
     * 
     * @return beschreibungenSet Die Beschreibungen zu einem Loesungswort
     */
    public HashSet<String> getBeschreibungenSet() {
        return beschreibungenSet;
    }
    
    /** Das Fenster oeffnet sich in der Mitte des Bildschirms */
    private Tupel<Integer> getAufloesung(){
    	return new Tupel<Integer>(400,300);
    }
    
    /**
     * Controller fuer diese Klasse, realisiert als private Klasse
     */
    private class BeschreibungListener implements ActionListener,FocusListener, ListSelectionListener{

        public void actionPerformed(ActionEvent action) {
            if (action.getSource() == buttonfeld[0]) {
                neueWortbeschreibung();
            } else if (action.getSource() == buttonfeld[1]) {
                editBeschreibung();
            } else if (action.getSource() == buttonfeld[2]) {
                loescheBeschreibung();
            } else if (action.getSource() == buttonfeld[3]) {
                bestaetigen();
            } else if (action.getSource() == buttonfeld[4]) {
                abbrechen();
            } else if (action.getSource() == wortTextfeld) {
                pruefeEingegebenesWort((JTextField) action.getSource());
            } else if (action.getSource() == wortbeschreibungsfeld) {
                pruefeBeschreibung((JTextField) action.getSource());
            }        
        }
        
        public void focusGained(FocusEvent e) {}

        public void focusLost(FocusEvent focus) {
            if (focus.getSource() == wortTextfeld) {
            	
            	/* Ist eingegebenes Loesungswort gueltig? */
                pruefeEingegebenesWort(wortTextfeld);
            } else if (focus.getSource() == wortbeschreibungsfeld) {
            	
            	/* Ist eingegebene Wortbeschreibung gueltig? */
                pruefeBeschreibung(wortbeschreibungsfeld);
            }
        }
        
        public void valueChanged(ListSelectionEvent e) {
            JList<String> liste = (JList<String>) e.getSource();
            
            /* Wenn Liste leer ist... */
            if (liste.isSelectionEmpty()) {
            	buttonfeld[1].setEnabled(false);
            	buttonfeld[2].setEnabled(false);            
            } else {
            	buttonfeld[1].setEnabled(true);
                
                /* Wenn eine Beschreibung vorhanden ist, also das
                 * Listenmodell mindestens ein Eintrag enthaelt */
                if (beschreibungsListenmodell.getSize() > 1) {
                	buttonfeld[2].setEnabled(true);
                } else {
                	buttonfeld[2].setEnabled(false);
                }
                wortbeschreibungsfeld.setText(liste.getSelectedValue());                          
            }
        }      
    }
}
