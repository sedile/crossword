package gui;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.WindowConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import steuerung.Tupel;
import steuerung.Wortliste;
import ausnahmen.KeinWortException;
import ausnahmen.SyntaxException;

/**
 * Hier hat der Benutzer die Moeglichkeit ein Raetsel per Wortauswahl selber zu generieren,
 * indem mehrere Woerter aus einer bestehenden Wortliste ausgewaehlt werden. Die Anzahl der Fuellwoerter 
 * werden ebenfalls vom Benutzer erfragt.
 */
public class GenAuswahlDialog extends JDialog {  
	
	/**
	 * Die komplette Wortliste
	 */
    private Wortliste wortliste;
    
    /**
	 * Entfernte Woerter aus der Auswahlliste werden wieder zurueck in die
	 * Wortliste eingefuegt, die vorher geladen wurde.
     */
    private Wortliste wortlisteKopie;
    
    /**
     *  ausgewaehlte Loesungswoerter werden spaeter hier eingefuegt zum Generieren
     */
    private Wortliste wortAusgewaehlt = new Wortliste();
	
    /** Feld enthaelt JButtons, wobei 0 = neues Wort, 1 = wort entfernen, 2 = bestaetigen , 3 = abbrechen */
    private JButton[] buttonfeld = new JButton[4];
    private JTextField fwort;
    private JLabel wortsuche, auswahlwoerter,fuellwortlabel;
    
    /* zur Anzeige und Auswahl der einzelnen Wprtern */
    private JList<String> wortwahlliste,auswahlliste;
    
    /* verwaltet die Eintraege einer JList */
    private DefaultListModel<String> wortwahlListenmodell = new DefaultListModel<String>();
    private DefaultListModel<String> auswahlListenmodell = new DefaultListModel<String>();
   
    private JTextField filterTextfeld = new JTextField(20); 
    
    /** Anzahl der Fuellwoerter */
    private short fuellwoerter;
    
    /**
     * ob ein Kreuzwortraetsel generiert werden soll
     */
    private boolean sollGenerieren = false;
    
    /**
     * Der Konstruktor baut das Fenster mit der uebergebenen Wortliste auf
     * 
     * @param frame  Damit keine zwei Fenster parallel bedient werden koennen
     * @param wListe Wortliste, aus der das Kreuzwortraetsel generiert werden soll
     */
    public GenAuswahlDialog(JFrame frame, Wortliste wListe) {
        super(frame, "Interaktiv-Modus", true);
        wortlisteKopie = wListe.getKopie();
        wortliste = wListe.getKopie();
        erzeugeFenster();
        pack();
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setLocation(getAufloesung().getLinks(), getAufloesung().getRechts());
        
        GenListener listener = new GenListener();
        listenerInit(listener, listener, listener);
    }
    
    /**
     * Fuegt allen Swing-Komponenten einen Listenerobjekt hinzu
     * @param action	Ein Actionlistener-Objekt
     * @param llistener	Ein ListSelectionListener-Objekt
     * @param focus		Ein Focuslistener-Objekt
     */
    private void listenerInit(ActionListener action, ListSelectionListener llistener, FocusListener focus){
    	for(byte i = 0; i < buttonfeld.length; i++){
    		buttonfeld[i].addActionListener(action);
    	}
        filterTextfeld.addActionListener(action);
        fwort.addActionListener(action);
        
        wortwahlliste.addListSelectionListener(llistener);
        auswahlliste.addListSelectionListener(llistener);
        
        filterTextfeld.addFocusListener(focus);
    }
    
    /**
     * erzeugt die Fensterdarstellung
     */
    private void erzeugeFenster() {
    	initKomponenten();
        setLayout(new BorderLayout());
        erzeugePanel();
    }
    
    /**
     * initialisiert die ganzen Swing-Komponenten
     */
    private void initKomponenten(){
    	/* Buttons initialisieren */
        buttonfeld[0] = new JButton("Hinzufuegen");
        buttonfeld[1] = new JButton("Entfernen");
        buttonfeld[2]  = new JButton("Bestaetigen");
        buttonfeld[3] = new JButton("Abbrechen");
        fwort = new JTextField(5);
        wortsuche = new JLabel("Wortsuche :");
        auswahlwoerter = new JLabel("ausgewaehlte Woerter fï¿½r das Raetsel");
        fuellwortlabel = new JLabel("Anzahl Fuellwoerter");
        
        /* zur Anzeige und Auswahl der einzelnen Woertern */
        wortwahlliste = new JList<String>();
        auswahlliste = new JList<String>();
    }

    /**
     * hier wird die Listendarstellung erzeugt
     * 
     * @param Panel mit ein Layoutmanager
     */
    private void erzeugePanel() {
    	JPanel panel = new JPanel();
        panel.setLayout(new BorderLayout());
        wortwahlListenmodell = new DefaultListModel<String>();
        wortwahlliste = new JList<String>(wortwahlListenmodell);
        wortwahlliste.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        wortwahlliste.setSelectedIndex(0);
        wortwahlliste.setVisibleRowCount(10);        
        aktualisiereListenModelle();
        
        JPanel filterListenPanel = new JPanel();
        filterListenPanel.setLayout(new BoxLayout(filterListenPanel, BoxLayout.Y_AXIS));
        JPanel filterPanel = new JPanel();
        filterPanel.add(wortsuche);
        filterPanel.add(filterTextfeld);
        filterListenPanel.add(filterPanel);
        filterListenPanel.add(new JScrollPane(wortwahlliste));
        panel.add(filterListenPanel, BorderLayout.NORTH);
        
        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 0;
        buttonPanel.add(buttonfeld[0], gbc);
        gbc.gridx++;
        buttonPanel.add(buttonfeld[1], gbc);
        gbc.gridx--;
        gbc.gridy++;
        buttonPanel.add(fuellwortlabel, gbc);
        gbc.gridx++;
        buttonPanel.add(fwort, gbc);
        fwort.setText("0");
        gbc.gridx--;
        gbc.gridy++;
        buttonPanel.add(buttonfeld[2], gbc);
        gbc.gridx++;
        buttonPanel.add(buttonfeld[3], gbc);
        gbc.gridx--;
        gbc.gridy++;
        buttonPanel.add(auswahlwoerter, gbc);
        panel.add(buttonPanel, BorderLayout.CENTER);
        add(panel, BorderLayout.CENTER);

        auswahlListenmodell = new DefaultListModel<String>();
        auswahlliste = new JList<String>(auswahlListenmodell);
        auswahlliste.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        auswahlliste.setSelectedIndex(0);
        auswahlliste.setVisibleRowCount(10);
        JPanel auswahlListenPanel = new JPanel();
        auswahlListenPanel.setLayout(new BoxLayout(auswahlListenPanel, BoxLayout.Y_AXIS));
        auswahlListenPanel.add(auswahlwoerter);
        auswahlListenPanel.add(new JScrollPane(auswahlliste));
        panel.add(auswahlListenPanel, BorderLayout.SOUTH);
        
        buttonfeld[0].setEnabled(false);
        buttonfeld[1].setEnabled(false);
    }
    
    /**
     * hier werden beide Listeninhalte aktualisiert, damit
     * aktuelle Woerter angezeigt werden und keine Duplikate gewaehlt werden
     * koennen
     */
    private void aktualisiereListenModelle() {
    	/* Woerter die zur Verfuegung stehen */
        wortwahlListenmodell.clear();
        ArrayList<String> filterliste = wortliste.getWortsuche(filterTextfeld.getText());
        for (int i=0; i < filterliste.size(); i++) {
            wortwahlListenmodell.addElement(filterliste.get(i));
        }
        
        /* Woerter die fuer das Raetsel ausgewaehlt wurden */
        auswahlListenmodell.clear();
        ArrayList<String> wortwahlliste = wortAusgewaehlt.getWortsuche(filterTextfeld.getText());
        for (int i=0; i < wortwahlliste.size(); i++) {
            auswahlListenmodell.addElement(wortwahlliste.get(i));
        }
    }
    
    /**
     * fuege die ausgewaehlten Woerter in die Liste ein die im Raetsel vorkommen
     * sollen und loescht diese Zugleich aus der vorhandenen Wortliste, um keine 
     * Duplikate auswaehlen zu koennen.
     */
    private void wortHinzufuegen() {
    	String auswahl = wortwahlliste.getSelectedValue();
    	
        /* Loesungswort hat nur eine Beschreibung. Keine Extraabfrage noetig */
        if (wortliste.getBeschreibung(auswahl).size() == 1) {
        	try {
        		wortAusgewaehlt.wortHinzufuegen(auswahl, wortliste.getBeschreibungenSet(auswahl));
                }
            catch (Exception e) {}
                
            wortliste.wortLoeschen(auswahl);
                
        } else {
         /* Extraabfrage noetig, wegen mehr ale eine Beschreibung */
            waehleBeschreibungZuLoesungwort(auswahl);
        }
        aktualisiereListenModelle(); 
    }
    
    /**
     * Umkehrung von <code>wortHinzufuegen</code>. Loescht Woerter aus der
     * Liste der ausgewaehlten Woerter und fuegt Sie wieder in die vorhandene
     * Wortliste wieder ein.
     */
    private void entferneWoerterAusAuswahlliste() {
    	String auswahl = auswahlliste.getSelectedValue();
        try {
        	/* Fuege Wort wieder in die Ursprungliste zurueck */
            wortliste.wortHinzufuegen(auswahl, wortlisteKopie.getBeschreibungenSet(auswahl));
        } catch (Exception e) {}
        
        wortAusgewaehlt.wortLoeschen(auswahl);
        aktualisiereListenModelle();
    }
    
    /**
     * Oeffnet ein Auswahldialog, falls zu einem Loesungswort mehrere 
     * Wortbeschreibungen vorhanden sind. Der Benutzer muss sich fuer
     * eine Wortbeschreibung entscheiden oder abbrechen.
     * 
     * @param loesungswort Das Loesungswort, fuer das die Beschreibung ausgewaehlt werden soll
     */
    private void waehleBeschreibungZuLoesungwort(String loesungswort) {
        WortbeschreibungDialog v = new WortbeschreibungDialog(GenAuswahlDialog.this,loesungswort, wortliste.getBeschreibungenSet(loesungswort));
        
        /* Auswahldialog anzeigen */
        v.setVisible(true);
        if (v.getSpeichererlaubnis() == true) {
            try {
                wortAusgewaehlt.wortHinzufuegen(loesungswort, v.getBeschreibungSet());  
            } catch (Exception e) {}
            	wortliste.wortLoeschen(loesungswort);
        }
    }   
    
    /**
     * prpfe, ob das Eingabefeld eine Zahl als Eingabe enthaelt
     * 
     * @param textfeld Das zu pruefende Eingabefeld
     */
    private void pruefeEingabe(JTextField textfeld) {
        try {
            Integer.parseInt(textfeld.getText());
        } catch (NumberFormatException e1) {
            textfeld.setText("0");
            JOptionPane.showMessageDialog(null, "Keine Numerische Eingabe", "Anzahl", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    /**
     * filtert die Wortliste, je nach eingegebenen String.
     * 
     * @param textfeld String als Einagbe fuer das Suchfeld
     */
    private void wortsuche(JTextField textfeld) {
        String filterString = textfeld.getText().toUpperCase();
        if (filterString.isEmpty() == true) {
        	
        	/* Es wurde nach kein Wort gefiltert */
        	aktualisiereListenModelle();
        } else {
            try {
                wortliste.pruefeLoesungswort(filterString);
                textfeld.setText(filterString);
                aktualisiereListenModelle();
            }
            catch (KeinWortException e1) {}
            catch (SyntaxException e2) {
                JOptionPane.showMessageDialog(null, "Filter enthaelt unzulaessiges Zeichen: " + e2.getMessage(), "Filter", JOptionPane.ERROR_MESSAGE);
            }
        }
    }
    
    /**
     * pruefe die Eingabe und beende den Dialog mit der Bestaetigung des
     * OK-Button
     * 
     * @throws NumberFormatException Es wurde keine Zahl eingegeben
     */
    private void bestaetigen() throws NumberFormatException {
        if (wortAusgewaehlt.getAnzahlLoesungswoerter() == 0) {
            JOptionPane.showMessageDialog(null, "Es muss mindestens 1 Wort ausgewaehlt werden", "Anzahl", JOptionPane.ERROR_MESSAGE);                        
            return;                    
        }
        
        /* Fuellwoerter */
        try {
            fuellwoerter = Short.parseShort(fwort.getText());
            
            if ( fuellwoerter < 0){
            	 JOptionPane.showMessageDialog(null, "Zahl muss groessergleich 0 sein.", "Anzahl", JOptionPane.ERROR_MESSAGE);
            	 return;
            }
            
        } catch (NumberFormatException n) {
            fwort.setText("0");
            JOptionPane.showMessageDialog(null, "Anzahl muss numerisch sein.", "Anzahl", JOptionPane.ERROR_MESSAGE);
            throw new NumberFormatException();
        }           
        sollGenerieren = true;
        setVisible(false);
    }

    /**
     * beendet den Dialog und es wird kein Raetsel generiert
     */
    private void abbrechen() {
        sollGenerieren = false;
        setVisible(false);
        dispose();
    }
    
    /**
     * gibt die Wortliste zurueck die alle Woerter enthaelt, minus
     * die ausgewaehlten Woerter
     * 
     * @return restliche Wortliste
     */
    public Wortliste getRestWortliste(){
    	return wortliste;
    }
    
    /**
     * gibt die Wortliste zurueck, die die Woerter enthaelt die
     * auf jeden Fall im Raetsel vorkommen sollen
     * 
     * @return ausgewaehlte Woerter
     */
    public Wortliste getWortAusgewaehltListe(){
    	return wortAusgewaehlt;
    }
    
    /**
     * gibt die Anzahl der Fuellwoerter zurueck
     * 
     * @return Anzahl der Fuellwoerter
     */
    public short getAnzahlFuellWoerter(){
    	return fuellwoerter;
    }
    
    /**
     * gibt die boolesche Variable zurueck, ob das
     * Raetsel generiert werden kann.
     * 
     * @return true oder false
     */
    public boolean getErlaubnisZumGenerieren(){
    	return sollGenerieren;
    }
    
    /** Das Fenster oeffnet sich in der Mitte des Bildschirms */
    private Tupel<Integer> getAufloesung(){
    	return new Tupel<Integer>(400,300);
    }
    
    /**
     * Controller als innere Klasse
     */
    private class GenListener implements ActionListener,FocusListener,ListSelectionListener {
       
    	public void actionPerformed(ActionEvent action) {
    		 if (action.getSource() == buttonfeld[0]) {
                 wortHinzufuegen();
             } else if (action.getSource() == buttonfeld[1]) {
                 entferneWoerterAusAuswahlliste();
             } else if (action.getSource() == buttonfeld[2]) {
            	 try {
            		 bestaetigen();
            	 }
            	 catch (NumberFormatException number) {}
                
            } else if (action.getSource() == buttonfeld[3]) {
                abbrechen();
            } else if (action.getSource() == filterTextfeld) {
                wortsuche((JTextField) action.getSource());
            } else if (action.getSource() == fwort) {
                pruefeEingabe((JTextField) action.getSource());
            }
        }
    	
        public void focusGained(FocusEvent e) {}

        public void focusLost(FocusEvent e) {
            if (e.getSource() == filterTextfeld) {
                wortsuche((JTextField)e.getSource());
            }
        }
        
        public void valueChanged(ListSelectionEvent e) {
            JList<String> liste = (JList<String>) e.getSource();
            if (liste == wortwahlliste) {
                if (wortwahlliste.isSelectionEmpty()) {
                	
                	/* Es muss ein Element ausgewaehlt werden was der
                	 * Auswahlliste hinzugefuegt wird */
                	buttonfeld[0].setEnabled(false);
                } else {
                	buttonfeld[0].setEnabled(true);
                }        
            } else {
                if (auswahlliste.isSelectionEmpty()) {
                	
                	/* Es muss ein Wort in der Auswahlliste
                	 * vorhanden sein */
                	buttonfeld[1].setEnabled(false);
                } else {
                	buttonfeld[1].setEnabled(true);
                }
            }
        }
    }
}
