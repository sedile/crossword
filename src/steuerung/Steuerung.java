package steuerung;

import generator.Generator;
import generator.Raetselbauer;
import gui.GenAuswahlDialog;
import gui.GenZufallDialog;
import gui.RaetselDialog;
import gui.WortbeschreibungEditor;
import gui.WortlistenEditor;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;

import javax.swing.AbstractButton;
import javax.swing.JFileChooser;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import ausnahmen.BeschreibungException;
import ausnahmen.KeinWortException;
import ausnahmen.SyntaxException;

/**
 * Dies ist die Starterklasse dieser Anwendung. Ausserdem wird von hier aus alles
 * aufgerufen was mit Wortlisten und den Generator zu tun haben.
 */
public class Steuerung implements ActionListener, KeyListener, ListSelectionListener, FocusListener {
    
	public static void main (String[] args){
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                new Steuerung();
            }
        }); 
	}
	
	/* Bezugsobjekte */
    private WortlistenEditor wortlisteView;    
    private Wortliste wortliste = new Wortliste(); 
    private File aktuelleDatei;
    
    /**
     * Der Konstruktor der Steuerklasse. Es wird ein Wortlisteneditor erzeugt und
     * diese Instanz wird in der Steuerklasse verwendet, um eine Wortliste zu
     * editieren.
     */
    Steuerung() {
    	WortlistenEditor editor = new WortlistenEditor();
        wortlisteView = editor;
        wortlisteView.setListener(this,this,this);
    }

    /**
     * wird auf eine Komponente geklickt, so wird die entsprechende
     * Methode aufgerufen.
     */
    public void actionPerformed(ActionEvent action) {
        if (action.getSource() == wortlisteView.getMenueintraege()[0]) {
        	neueListe((AbstractButton) action.getSource());
        } else if (action.getSource() == wortlisteView.getMenueintraege()[1]) {
            listeLaden((AbstractButton) action.getSource());
        } else if (action.getSource() == wortlisteView.getMenueintraege()[2]) {
            listeSpeichern((AbstractButton)action.getSource());
        } else if (action.getSource() == wortlisteView.getMenueintraege()[3]) {
            beenden();
        } else if (action.getSource() == wortlisteView.getButtons()[0]) {
            wortHinzufuegen();
        } else if (action.getSource() == wortlisteView.getButtons()[1]) {
            wortBearbeiten();
        } else if (action.getSource() == wortlisteView.getButtons()[2]) {
            wortLoeschen();
        } else if (action.getSource() == wortlisteView.getButtons()[3]){
        	loescheWortliste();
        } else if (action.getSource() == wortlisteView.getWortsucheTextfeld()) {
            wortsuche((JTextField) action.getSource());
        } else if (action.getSource() == wortlisteView.getButtons()[4]) {
            generiereInteraktiv();
        } else if (action.getSource() == wortlisteView.getButtons()[5]) {
            generiereZufall();
        }       
    }

    public void keyPressed(KeyEvent e) {}

    public void keyReleased(KeyEvent e) {}

    public void keyTyped(KeyEvent e) {}

    /**
     * regelt ob die Buttons zum editieren der Wortliste anklickbar sind oder 
     * nicht, je nachdem, ob eine Wortliste leer ist.
     */
    public void valueChanged(ListSelectionEvent e) {
        JList<String> liste = (JList<String>) e.getSource();
        
        /* Es wurde kein Listenelement ausgewaehlt */
        if (liste.isSelectionEmpty() == true) {
            wortlisteView.getButtons()[1].setEnabled(false);
            wortlisteView.getButtons()[2].setEnabled(false); 
            wortlisteView.getButtons()[3].setEnabled(false);
        } else {
            wortlisteView.getButtons()[1].setEnabled(true);
            wortlisteView.getButtons()[2].setEnabled(true);  
            wortlisteView.getButtons()[3].setEnabled(true);
        }
    }
    
    public void focusGained(FocusEvent e) {}

    /**
     * Textfeld wo nach ein Wort gesucht werden kann
     */
    public void focusLost(FocusEvent focus) {
        if (focus.getSource() == wortlisteView.getWortsucheTextfeld()) {
            wortsuche((JTextField) focus.getSource());
        }
    }
    
    
 /**
  * hier wird der Generator ausgeloest, indem beliebige Woerter aus der
  * Wortliste rausgesucht werden koennen. Anschliessend wird das generierte
  * Raetsel in einem <code>Raetsel</code> dargestellt
  */
 private void generiereInteraktiv() {
     GenAuswahlDialog auswahl = new GenAuswahlDialog(wortlisteView, wortliste);
     auswahl.setVisible(true);
     zeigeGeneriertesRaetsel(auswahl);
 }

 /**
  * hier wird der Generator ausgeloest, indem zufaellig Woerter aus der
  * Wortliste rausgesucht werden koennen. Anschliessend wird das generierte
  * Raetsel in einem <code>Raetsel</code> dargestellt
  */
 private void generiereZufall() {
     GenZufallDialog zufall = new GenZufallDialog(wortlisteView, wortliste);
     zufall.setVisible(true);
     zeigeGeneriertesRaetsel(zufall);
 }
 
 /**
	 * Wenn Woerter per Zufall ausgewaehlt wurden, dann wird ein Generator erzeugt der fuer das
	 * generieren eines Raetsels zustaendig ist.
	 * 
  * @param zufall Um die Erlaubnis zum generieren zu bekommen.
  */
 private void zeigeGeneriertesRaetsel(GenZufallDialog zufall) {
 	/* wenn ein Raetsel generiert werden soll */
     if (zufall.getErlaubnisZumGenerieren() == true) {
     	/* uebergebe den Generator die komplette Wortliste, die ausgewaehlten Woerter zum generieren und die
     	 * Anzahl der Fuellwoerter */
     	 Generator g = new Generator(zufall.getWortAusgewaehltListe(), zufall.getRestWortliste(),zufall.getAnzahlFuellWoerter());
         Raetselbauer raetselgitter = g.generiere();
         RaetselDialog dialog = new RaetselDialog(wortlisteView, raetselgitter);
         dialog.setVisible(true);
         zufall.dispose();
     }
 }
 
 /**
	 * Wenn Woerter selbst ausgewaehlt wurden, dann wird ein Generator erzeugt der fuer das
	 * generieren eines Raetsels zustaendig ist.
	 * 
  * @param auswahl Um die Erlaubnis zum generieren zu bekommen.
  */
 private void zeigeGeneriertesRaetsel(GenAuswahlDialog auswahl) {
     if (auswahl.getErlaubnisZumGenerieren() == true) {
     	 Generator g = new Generator(auswahl.getWortAusgewaehltListe(),auswahl.getRestWortliste(),auswahl.getAnzahlFuellWoerter());
         Raetselbauer raetselgitter = g.generiere();
         RaetselDialog dialog = new RaetselDialog(wortlisteView, raetselgitter);
         dialog.setVisible(true);
         auswahl.dispose();
     }
 }
    
    /**
     * Falls eine Wortliste existiert, so wird diese geloescht und der
     * Benutzer hat die Moeglichkeit eine leere Wortliste eigenstaendig
     * mit Woertern zu befuellen
     * 
     * @param button die Swing-Komponente welches dieses Ereignis ausloest
     */
    private void neueListe(AbstractButton button){
    	wortlisteView.neuesListenmodell(new ArrayList<String>());
    	wortlisteView.getMenueintraege()[1].setEnabled(true);
    	loescheWortliste();
    }
   
    /**
     * Diese Methode steuert das Einladen einer neuen Wortliste aus einer
     * Datei. Es koennen mehrere Wortlisten hintereinander geladen werden.
     * 
     * @param button die Swing-Komponente fuer das Laden einer Liste
     */
    private void listeLaden(AbstractButton button) {
    	JFileChooser fc = new JFileChooser();
        if (aktuelleDatei != null) {
            fc = new JFileChooser(aktuelleDatei.getParentFile());
        }       
        
        int r = fc.showOpenDialog(button);
        if (r == JFileChooser.APPROVE_OPTION) {
            File datei = fc.getSelectedFile();
            try {
                wortlisteView.neuesListenmodell(listeOeffnen(datei));
            }
            catch (FileNotFoundException e1) {
                JOptionPane.showMessageDialog(null, "Datei nicht gefunden", "Fehler beim oeffnen der Datei", JOptionPane.ERROR_MESSAGE);
            }
            catch (IOException e2) {
                JOptionPane.showMessageDialog(null, "Dateizugriff fehlgeschlagen", "Fehler beim oeffnen der Datei", JOptionPane.ERROR_MESSAGE);
            }
        }
        
        /* Es kann nun ein Raetsel generiert werden, wenn die Wortliste Woerter enthaelt */
        if (wortliste.getAnzahlLoesungswoerter() > 0){
            wortlisteView.getButtons()[4].setEnabled(true);
            wortlisteView.getButtons()[5].setEnabled(true);
        } else {
            wortlisteView.getButtons()[4].setEnabled(false);
            wortlisteView.getButtons()[5].setEnabled(false);
        }
    }
    
    /**
     * hier wird eine Wortliste aus einer Datei eingeladen und uebergibt die Datei 
     * als Parameter an <code>Wortliste</code>, um alle Woerter aus dieser Datei
     * einzulesen und zu speichern.
     * 
     * @param datei einzuladene Datei
     * @return ArrayList was alle Loesungswoerter aus der Textdatei enthaelt
     * @throws FileNotFoundException	Keine Datei gefunden
     * @throws IOException	Einlesefehler
     */
    private ArrayList<String> listeOeffnen(File datei) throws FileNotFoundException, IOException{
        wortliste.ladeListe(datei);
        aktuelleDatei = datei;
        ArrayList<String> wortfeld = new ArrayList<String>();
        /* Wortsuchetextfeld leer machen, da zu Beginn nach allen Woerten gefiltert werden soll */
        wortfeld.addAll(wortliste.getWortsuche(""));
        return wortfeld;
    }
    
    /**
     * diese Methode ist dafuer zustaendig, dass die aktuelle Wortliste in
     * eine Datei gespeichert wird, die spaeter wieder eingelesen werden kann
     * 
     * @param button die Swing-Komponente fuer das Speichern der Wortliste
     */
    private void listeSpeichern(AbstractButton button) {
        JFileChooser fc = new JFileChooser();
        if (aktuelleDatei != null) {
            fc = new JFileChooser(aktuelleDatei.getParentFile());
        }   
        int rWert = fc.showSaveDialog(button);
        if (rWert == JFileChooser.APPROVE_OPTION) {
            File datei = fc.getSelectedFile();
            try {
            	
            	/* speichert die Liste ab */
                wortliste.speichereWortliste(datei);
                aktuelleDatei = datei;
                JOptionPane.showMessageDialog(null, "Wortliste wurde erfolgreich in\n"+datei.getAbsolutePath()+"\ngespeichert", "Speichern", JOptionPane.INFORMATION_MESSAGE);
            }
            catch (IOException e1) {
                JOptionPane.showMessageDialog(null, "Dateizugriff fehlgeschlagen", "Speichern", JOptionPane.ERROR_MESSAGE);
            }
        }
    }
    
    /**
     * beenden diesen Dialog und somit auch die Anwendung.
     * Das Fenster wird geschlossen.
     */
    private void beenden() {
        wortlisteView.setVisible(false);
        wortlisteView.dispose();
        System.exit(0);
    }
    
    /**
     * Es wird ein neues Loesungswort oder Wortbeschreibung in die Wortliste eingefuegt. Dazu wird ein
     * neues Fenster erzeugt, was dem Benutzer diverse Auswahlmoeglichkeiten zum Editieren
     * der Wortliste zur Verfuegung stellt.
     */
    private void wortHinzufuegen() { 
        WortbeschreibungEditor editor = new WortbeschreibungEditor(wortlisteView);
        editor.setVisible(true);
        if (editor.getErgebnis() == true) {            
            try {
                wortliste.wortHinzufuegen(editor.getLoesungswort(), editor.getBeschreibungenSet());               
                ArrayList<String> wortfeld = new ArrayList<String>();
                wortfeld.addAll(wortliste.getWortsuche(""));
                wortlisteView.neuesListenmodell(wortfeld);
            }
            catch (KeinWortException e1) {}
            catch (SyntaxException e2) {}
            catch (BeschreibungException e3) {}          
        }
        
        if (wortliste.getAnzahlLoesungswoerter() > 0){
            wortlisteView.getButtons()[4].setEnabled(true);
            wortlisteView.getButtons()[5].setEnabled(true);
        } else {
            wortlisteView.getButtons()[4].setEnabled(false);
            wortlisteView.getButtons()[5].setEnabled(false);
        }
    }
    
    /**
     * manipluiert ein Wort, indem es die Hilfe von <code>WortbeschreibungEditor</code>
     * in Anspruch nimmt. Es wird ein neues Fenster zum Editieren der Wortliste geoeffnet.
     */
    private void wortBearbeiten() {
        String aktuellesWort = wortlisteView.getWortliste().getSelectedValue();
        HashSet<String> beschreibungen = wortliste.getBeschreibungenSet(aktuellesWort);
        
        /* uebergebe das ausgwaehlte Loesungswort samt seiner Beschreibungen */
        WortbeschreibungEditor editor = new WortbeschreibungEditor(wortlisteView, aktuellesWort, beschreibungen);
        editor.setVisible(true);
        if (editor.getErgebnis() == true) {
            try {
            	/* "ueberschreiben" */
            	wortliste.wortLoeschen(editor.getLoesungswort());
                wortliste.wortHinzufuegen(editor.getLoesungswort(), editor.getBeschreibungenSet());
            }
            catch (KeinWortException e1) {}
            catch (SyntaxException e2) {}
            catch (BeschreibungException e3) {}          
        }
    }
    
    /**
     * loescht das aktuell ausgewaehlte Wort aus der aktuellen Wortliste
     */
    private void wortLoeschen() {
    	String wort = wortlisteView.getWortliste().getSelectedValue();
        int wortindex = wortlisteView.getWortliste().getSelectedIndex();
        wortliste.wortLoeschen(wort);
        wortlisteView.getWortlistenmodell().remove(wortindex);
    
        if (wortliste.getAnzahlLoesungswoerter() > 0){
            wortlisteView.getButtons()[4].setEnabled(true);
            wortlisteView.getButtons()[5].setEnabled(true);
        } else {
            wortlisteView.getButtons()[4].setEnabled(false);
            wortlisteView.getButtons()[5].setEnabled(false);
        }
    
    }
    
    /**
     * loescht die aktuelle Wortliste vollstaendig
     */
    private void loescheWortliste(){
    	wortliste.wortlisteLoeschen();
    	wortlisteView.getWortlistenmodell().removeAllElements();
    	wortlisteView.getButtons()[4].setEnabled(false);
    	wortlisteView.getButtons()[5].setEnabled(false);
    }
    
    /**
     * regelt das Suchen nach Woertern in der Wortliste. Es wird ein Filter 
     * als String realisiert, um so Loesungswoerter leichter zu finden. Ist der 
     * Filter leer, so werden alle Woerter "gesucht". Ist der Filter nicht leer, 
     * so werden die passenden Loesungswoerter ermittelt und angezeigt.
     * 
     * @param textfeld die Swing-Komponente des JTextField
     */
    private void wortsuche(JTextField textfeld) {
        String suche = textfeld.getText().toUpperCase();
        if (suche.isEmpty() == true) {

        	/* Suche nach allen Woertern, da Suchfeld leer ist */
            ArrayList<String> wortsucheliste = wortliste.getWortsuche(suche);
            wortlisteView.neuesListenmodell(wortsucheliste);
        } else {
            try {
            	
            	/* Suche nach den String der in das Suchfeld eingegeben wurde und
            	 * pruefe, ob die Eingabe akzeptabel ist */
                suche = wortliste.pruefeLoesungswort(suche);
                textfeld.setText(suche);
                ArrayList<String> wortsucheliste = wortliste.getWortsuche(suche);
                wortlisteView.neuesListenmodell(wortsucheliste);
            }
            catch (KeinWortException e1) {}
            catch (SyntaxException e2) {
                textfeld.setText("");
                JOptionPane.showMessageDialog(null, "Filter enthaelt unzulaessige Zeichen", "Filterfehler", JOptionPane.ERROR_MESSAGE);
            }
        }
    }
}
