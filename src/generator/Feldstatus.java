package generator;

/**
 * Speichert den Status den eine Feldposition haben kann
 */
public class Feldstatus {
    /**
     * Von dieser Position aus kann ein horizontales Wort platziert werden
     */
    public static final char HORIZONTAL = 'h';
    
    /**
     * Von dieser Position aus kann ein vertikales Wort platziert werden
     */
    public static final char VERTIKAL = 'v';
    
    /**
     * Hier kann entweder ein horizontales oder ein Vertikales Wort
     * entstehen, zudem kann eine Wortkreuzung entstehen.
     */
    public static final char BEIDE = '+';
    
    /**
     * stellt ein Feldelement mit einer Beschreibung dar.
     */
    public static final char BESCHREIBUNG = 'b';
    
    /**
     * hier gibt es weder ein Wort noch eine Wortbeschreibung
     */
    public static final char NICHTS = 'x';
    
    /**
     * initialies Feldelement.
     */
    public static final char UNGENUTZT = ' ';
    
    /**
     * Feld ist leer, koennte aber benutzt werden.
     */
    public static final char LEER = 'l';
}
