package generator;

import java.util.ArrayList;

import ausnahmen.FeldException;
import steuerung.Tupel;
import steuerung.Wortliste;

/**
 * Die Generatorklasse ist fuer das Erstellen eines Kreuzwortraetsels zustaendig. 
 * Neben der Wortplatzierung wird geprueft, ob und wo noch Fuellwoerter eingesetzt 
 * werden koennen.
 */
public class Generator {
	
	// Bezugsobjekt
	private Raetselbauer gitter;
	
	/** enthaelt die Woerter aus der Wortliste, die ausgewaehlt wurden
	  * zum generieren eines Kreuzwortraetsels */
	private Wortliste ausgewaehlt;
	
	/** enthaelt alle Woeter aus der Wortliste minus der ausgewaehlten Woerter */
	private Wortliste restliste;
    
	/** Ob eine Kreuzung eingefuegt werden soll */
    private boolean kreuzungErzwingen;
    
    /** Anzahl der Fuellwoerter */
    private short fuellwort;
    
    /** maximale Anzahl der Woerter in der Liste */
    private int maxWoerter;
    
    /** Wieviele Woerter in das Raetsel eingefuegt wurden */
    private short eingefuegt;
    
    /** Anzahl der horizontalen Woerter */
    private short horizontal;
    
    /** Anzahl der vertikalen Woerter */
    private short vertikal;
    
    /**
     * Der Konstruktor erzeugt einen neuen Generator. Dabei wird eine Wortliste uebergeben
     * die die Pflichtwoerter enthaelt, die auf jeden Fall im Raetsel vorkommen muessen und einer
     * weitere Wortliste, die alle Woerter minus der Pflichtwoerter enthaelt, um evtl. noch
     * Fuellwoerter einzufuegen. Die zweite Wortliste muss vorhanden sein, um Duplikate zu meiden
     * 
     * @param wListePflicht Liste aller Woerter die im Raetsel vorkommen sollen
     * @param wListeRest Wortliste mit allen Woertern, allerdings ohne die Pflichtwoerter, die bereits
     * im Raetsel vorkommen
     * @param anzahlFuellwoerter maximale Anzahl an Fuellwoertern, um das Raetsel aufzufuellen
     */
    public Generator(Wortliste wListePflicht, Wortliste wListeRest, short anzahlFuellwoerter) {
        ausgewaehlt = wListePflicht;
    	restliste = wListeRest;
        fuellwort = anzahlFuellwoerter;
        maxWoerter = wListePflicht.getAnzahlLoesungswoerter();
        kreuzungErzwingen = false;
    }
    
    /**
     * hier wird das Kreuzwortraetsel generiert. Es wird dabei die Klasse
     * <code>Raetselbauer</code> aufgerufen und dem Konstruktor eine maximale
     * Raetselbreite- und hoehe zu uebergeben, die sich aus der Gesamtlaenge aller
     * Buchstaben der ausgewaehlten Loesungswoerter ergibt. Dieser Wert wird durch
     * ein Skalar dividiert und scheint fuer eine initiale Raetselgroesse geeignet zu 
     * sein, die spaeter noch gekuerzt wird, um ein Raetsel so klein wie moeglich zu 
     * erzeugen.
     * 
     * @return das erzeugte Kreuzwortraetsel
     */
    public Raetselbauer generiere() {  	
    	int skalar = getSkalar();
        gitter = new Raetselbauer(ausgewaehlt.getGesamteWortLaenge()/skalar, ausgewaehlt.getGesamteWortLaenge()/skalar);
        int zaehler = ausgewaehlt.getAnzahlLoesungswoerter();
        
        /* Der Generator laeuft solange, bis alle ausgewaehlten
         * Woerter in das Raetsel eingefuegt wurden 
         * (Hier ohne Fuellwoerter)
         */             
        do {
        	erzeugeRaetsel();    
        	/* Es wurde kein Wort eingefuegt, da eine Kreuzung in ein Durchlauf unmoeglich ist */
            if (ausgewaehlt.getAnzahlLoesungswoerter() == zaehler) {
                kreuzungErzwingen = false;
            } else {
                zaehler = ausgewaehlt.getAnzahlLoesungswoerter();
                kreuzungErzwingen = true;
            }
        } while (ausgewaehlt.getAnzahlLoesungswoerter() > 0);
        
        /* kleinstmoegliches Raetsel wird uebergeben und mit Fuellwoertern
         * aufgefuellt, falls erwuenscht */
        Tupel<Integer> grenze = gitter.getNeueGrenzen();
        gitter = gitter.getKleinstesRaetsel(grenze);
        
        if ( fuellwort > 0){
            raetselAuffuellen();
        }
        return gitter;
    }
    
    /**
     * Durchsucht die Woerter in der Wortliste, um Sie in das Raetselgitter platzieren zu koennen.
     * Dabei verlaeuft die Platzsuche Diagonal beginnend von der oberen linken Ecke aus durch das ganze Raetselgitter
     * bis hin zur unteren rechten Ecke, und zwar solange bis es keine Woerter mehr zum
     * einfuegen gibt. Eventuell wird diese Methode mehrfach aufgerufen, falls waehrend eines Durchlaufes
     * keine Wortkreuzung moeglich war.<br>
     * Grob gesagt werden dazu zunaechst die einzelnen Feldkoordinaten ermittelt und danach in welcher
     * Richtung ein Wort verlaufen kann (<code>pruefeMoeglicheStartrichtung</code>). Danach wird ein passendes 
     * Loesungswort ermittelt (<code>getMuster</code> und <code>getMustertreffer</code>), danach
     * eine Wortbeschreibung zu den ermittelten Loesungswort (<code>getBeschreibung</code> und <code>getIndex</code>) und abschlieï¿½end die 
     * Position der Wortbeschreibung (<code>beschreibungEinfuegen</code> und <code>wortEinfuegen</code>).
     */
    private void erzeugeRaetsel() {
    int diagonal = gitter.getBreite() + gitter.getHoehe() - ausgewaehlt.getKuerzestesWort();
        for (short aktZeile = 1; aktZeile < diagonal; aktZeile++) {
        	for (int zeile = aktZeile, spalte = 0; zeile >= 0; zeile--, spalte++) {
                if (zeile < gitter.getHoehe() && spalte < gitter.getBreite()) {
                	Tupel<Integer> startpunkt = new Tupel<Integer>(zeile,spalte);                   
                    
                    /* Pruefe die Richtung wie ein Loesungswort verlaufen kann */
                    char richtung = gitter.pruefeMoeglicheStartrichtung(startpunkt);                   
                    if (richtung == Feldstatus.HORIZONTAL || richtung == Feldstatus.VERTIKAL) {
                        wortsuche(startpunkt, richtung);
                    }

                    /* Es kann eine Wortkreuzung ermoeglicht werden */
                    if (richtung == Feldstatus.BEIDE) {
                    	
                    	/* Wenn es mehr vertikale Woerter als horizontale Woerter gibt und
                    	 * keine Wortkreuzung eingefuet werden konnte */
                        if (horizontal <= vertikal) {
                            if (wortsuche(startpunkt, Feldstatus.HORIZONTAL) == false) {
                            	wortsuche(startpunkt, Feldstatus.VERTIKAL);
                            }
                        } else if (horizontal > vertikal){
                            if (wortsuche(startpunkt, Feldstatus.VERTIKAL) == false) {
                            	wortsuche(startpunkt, Feldstatus.HORIZONTAL);
                            } 
                        }
                    }
                }
                if (eingefuegt == maxWoerter) 
                	break;
            }
        	// System.out.println("Noch "+(maxWoerter-eingefuegt)+" Woerter einfuegen");
        	if (eingefuegt == maxWoerter)
            	break;
        }
    }
    
    /**
     * Da hier eine moegliche Startposition feststeht, wird versucht ein Wort an 
     * dieser Stelle zu platzieren. Hier steht auch bereits fest, in welcher Richtung
     * ein Wort eingefuegt werden kann, entweder in horizontaler oder in vertikaler 
     * Richtung. Dies wurde in <code>pruefeMoeglicheStartrichtung</code> ermittelt.
     * 
     * @param startpunkt Startkoordinate des Loesungswortes
     * @param richtung Richtung des Loesungswortes (horizontal oder vertikal)
     * @return true, wenn das Einfuegen geklappt hat, sonst false
     */

    private boolean wortsuche(Tupel<Integer> startpunkt, char richtung) {
    	ArrayList<String> wort = sortiereWoerter();
        boolean aktWortEingefuegt = false;
        for (int t = 0; t < wort.size(); t++) {
        	/* Pruefe, fuer jeden Arrayeintrag, ob ein Loesungswort in dieser Position
        	 * in das Raetsel horizontal oder vertikal eingefuegt werden kann */
            if (gitter.pruefeObWortPasst(startpunkt, wort.get(t), richtung) == true) {

            	/* Wenn bereits ein Wort eingefuegt wurde aber kein anderes Wort hat eine Buchstabengemeinsamkeit 
            	 * mit den eingefuegten Wort => keine Wortkreuzung, neue Startkoordinate nehmen und von dort aus pruefen */
                if (kreuzungErzwingen == true & gitter.pruefeZeichenGemeinsamkeit(startpunkt, wort.get(t), richtung) == 0 ) {
                    break;
                }
                try {                	
                	/* Zu dem Loesungswort was eingefuegt werden kann wird noch eine Beschreibung hinzugefuegt und die
                	 * Beschreibungsposition festgelegt */
                	String wortbeschreibung = ausgewaehlt.getBeschreibung(wort.get(t)).get(0);
                    gitter.beschreibungEinfuegen(startpunkt, wort.get(t), richtung, wortbeschreibung);
                    /* Wort aus Wortliste loeschen, um duplikat zu vermeiden */
                    ausgewaehlt.wortLoeschen(wort.get(t));
                    
                    /* Zaehle die Anzahl der horizontalen/vertikalen Woerter */
                    if (richtung == Feldstatus.HORIZONTAL) {
                        horizontal++;
                    } else if (richtung == Feldstatus.VERTIKAL){
                        vertikal++;
                    }

                    eingefuegt++;
                    kreuzungErzwingen = true;
                    aktWortEingefuegt = true;
                    break;
                }
                
                catch (ArrayIndexOutOfBoundsException ae){}
                catch (FeldException fe) {}
            } else {          	
            	/* Wort wurde nicht eingefuegt */
            	aktWortEingefuegt = false;
            }
        }
        return aktWortEingefuegt;
    }
    
    /**
     * hier werden die Fuellwoerter eingefuegt. Die Quelle stammt aus
     * der Wortliste die alle Woeter aus der Wortliste minus der 
     * ausgewaehlten Woerter enthaelt, um Duplikate zu vermeiden
     */
    private void raetselAuffuellen() {
        ausgewaehlt = restliste;
        maxWoerter = fuellwort;
        eingefuegt = 0; // Zu 0 setzen, da ab hier nur noch Fuellwoerter eingefuegt werden
        short zaehler = 0;
        
        /* Solange es noch Fuellwoerter zum einfuegen gibt */
        do {
        	erzeugeRaetsel();
            if (zaehler == eingefuegt) {
                break;
            } else {
                zaehler = eingefuegt;
            }
        } while (eingefuegt <= maxWoerter);
    }
    
    /**
     * sortiert alle ausgewaehlten Woerter in der Wortliste absteigend nach
     * ihrer laenge. Das erste Element der Liste wird das laengste Wort und
     * das letzte Element in der Liste das kuerzeste Wort sein
     * @return sortierte Woerter nach ihrere laenge
     */
    private ArrayList<String> sortiereWoerter(){
    	ArrayList<String> sortierteListe = new ArrayList<String>();
    	int maxWortlaenge = ausgewaehlt.getLaengstesWort();
    	int minWortlaenge = ausgewaehlt.getKuerzestesWort();
    	
    	/* zuerst das laengste Wort und dann immer ein Zeichen weniger */
    	for(int l = maxWortlaenge; l >= minWortlaenge; l--){
    		for(String wort : ausgewaehlt.getWoerterliste().keySet()){
    			if ( wort.length() == l){
    				sortierteListe.add(wort);
    			}
    		}
    	}
    	return sortierteListe;
    }
    
    /**
     * Ermittelt je nach Anzahl der Woerter im Raetsel ein passenden Skalar, damit
     * die initialie Raetselgroesse moeglichst klein bleibt
     * @return skalar, um die Raetselgroesse zu beeinflussen
     */
    private int getSkalar(){	
        if (maxWoerter <= 10){
        	return 1;
        } else if (maxWoerter > 10 & maxWoerter <= 50){
        	return 5;
        } else if (maxWoerter > 50 & maxWoerter <= 250) {
        	return (maxWoerter/10);
        } else if (maxWoerter > 250){
        	return (maxWoerter/(5*ausgewaehlt.getKuerzestesWort()));
        }
        return 1;
    }
}
