package ausnahmen;

/**
 * Diese Exception geworfen, wenn es eine leerzeile beim einlesen gibt, oder
 * wenn zu einer Beschreibung kein Loesung geben wird
 */
 public final class KeinWortException extends Exception {
	/**
	 * Aufruf des Konstruktors mit dem zu
	 * erscheinenden Fehlertext
	 * 
	 * @param text Fehlertext
	 */
	public KeinWortException(String text){
		super(text);
	}
}
